import React, {useState} from "react";
import AddTaskForm from "./components/AddTaskForm";
import Task from "./components/Task";
import './App.css';

const App = () => {
  let taskHolder;

  const [tasks, setTasks] =useState([
      {task : 'Finish toDoList', id : 0},
      {task : 'Upload to BitBucket', id : 1}
  ]);
  const [id, setId] = useState(2);
  const addTask = newTask => {
        taskHolder = newTask;
  };

  const removeTask = id =>{
      const index = tasks.findIndex(task => task.id === id);
      const tasksCopy = [...tasks];
      tasksCopy.splice(index, 1);
      setTasks(tasksCopy);

  };

  const postTask = event => {
      event.preventDefault();
      if(taskHolder){
          let idCopy = id;
          const tasksCopy = [...tasks];
          let task = {task: taskHolder, id: idCopy}
          tasksCopy.push(task);
          setTasks(tasksCopy);
          idCopy++
          setId(idCopy);
      };
  };

  const tasksList = (
      <>
          {
              tasks.map((task) => {
                   return <Task
                       key ={task.id}
                       text = {task.task}
                       remove = {() => (removeTask(task.id))}

                   />
              })
          }

      </>
  )



  return(
      <div className = 'App'>
        <AddTaskForm
            changeTask = {addTask}
            handleClick = {event => postTask(event)}
        />
          {tasksList}
      </div>
  )
}

export default App;
